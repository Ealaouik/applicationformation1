package KotlinExample

import java.util.*

class PremierTestKotlin {
}
fun highOrderFunction (a:String, b:String, fn:(String,String)->String){
    var resultat = fn(a,b)
    println(resultat);
}
fun main(args:Array<String>){


    var functionLambda :(String, String) -> String = {
        param1:String, param2:String -> "test fonction lamda avec $param1 et $param2"
    }

    highOrderFunction("Test1", "Test2", functionLambda)

}