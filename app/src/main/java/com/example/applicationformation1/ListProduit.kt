package com.example.applicationformation1


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.bumptech.glide.Glide
import com.example.applicationformation1.adapters.ProduitAdapter
import com.example.applicationformation1.data.Produit
import com.example.applicationformation1.services.SharedPreference
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_list_produit.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class ListProduit(var actiCaller : GestionProduitActivity) : Fragment() {

    // will be used for pagination
    var page = 0;
    var productList = ArrayList<Produit>();
    var recyclerView : RecyclerView?=null;



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_produit, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView = view?.findViewById(R.id.listProduitRecyclerView);

        recyclerView?.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy<0){
                    Toast.makeText(activity, "You are scrolling up", Toast.LENGTH_SHORT).show()
                }
                else if (dy>0) {
                    if (page <7 ) {
                        page += 7
                        getListProduct()
                    }
                }
            }
        });
        recyclerView?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        getListProduct()
    }

    fun getListProduct() {
        listProductListProgressbar.visibility = View.VISIBLE;
        var listProductUrl = "http://nticformation.com/listProduct.php";
        val stringRequest =object : StringRequest(
            Request.Method.POST, listProductUrl,
            Response.Listener<String> { response ->
                try {
                    val objarray = JSONArray(response)
                    for (i in 0..objarray.length() - 1) {
                        var p=Produit()
                        var produitobject: JSONObject = objarray.getJSONObject(i)
                        //libelle,prix,marque,prixvente,qteStock,image,imagelist,description
                        p.libelle = produitobject.getString("libelle").toString()
                        p.marque = produitobject.getString("marque").toString()
                        p.prix = produitobject.getString("prix").toString().toFloat()
                        p.prix = produitobject.getString("prixvente").toString().toFloat()
                        p.stock = produitobject.getString("qteStock").toString().toFloat()
                        p.image = produitobject.getString("image").toString()
                        p.listImage = produitobject.getString("imagelist").toString()
                        p.description = produitobject.getString("description").toString()
                        p.code = produitobject.getString("code").toString()
                        p.id = produitobject.getString("id").toInt()
                        productList.add(p)
                    }

                    val adapter = ProduitAdapter(this,productList,actiCaller)
                    //now adding the adapter to recyclerview
                    recyclerView?.setAdapter(adapter)
                    //when we update recylerview or produitList we notify the adapter
                    adapter?.notifyDataSetChanged();
                    listProductListProgressbar.visibility=View.GONE

                } catch (e: JSONException){
                    e.printStackTrace();
                    Toast.makeText(activity, "Probleme de données", Toast.LENGTH_SHORT).show()
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                        .show()
                }
            } ) {
            override fun getParams(): MutableMap<String, String> {

                val params = HashMap<String, String>()
                params.put("page", ""+page)

                return params
            }
        }
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }
}
