package com.example.applicationformation1.kotlinFunctions

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.applicationformation1.R
import kotlinx.android.synthetic.main.activity_sensors.*

class SensorsActivity : AppCompatActivity(), SensorEventListener {


    private lateinit var mySensors :SensorManager
    private var lightSensor : Sensor? = null
    private var proximitySensor : Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensors)
        mySensors = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val deviceSensors = mySensors.getSensorList(Sensor.TYPE_ALL)
        listSensors.text = "Sensors size "+ deviceSensors.size

        deviceSensors.forEach{
            listSensors.text = listSensors.text.toString() +"\n "+it.name
        }

        lightSensor = mySensors.getDefaultSensor(Sensor.TYPE_LIGHT)
        proximitySensor = mySensors.getDefaultSensor(Sensor.TYPE_PROXIMITY)

    }

    fun checkSensorAvailabiliy() : Boolean{
        var lightAvailable = false
        if (lightSensor != null){
            lightAvailable = true;
        }
        return lightAvailable
    }

    override fun onSensorChanged(event: SensorEvent?) {

        if(event?.sensor?.type == Sensor.TYPE_LIGHT){
            /*val valeur = event.values[0]
            if (valeur>40){
                Log.e("sup 40" ,"valeur superieur à 40 "+ valeur)
            }else {
                Log.e("inf 40" ,"valeur inférieur à 40 "+ valeur)

            }*/
        } else if (event?.sensor?.type == Sensor.TYPE_PROXIMITY){
            val valeur = event.values[0]
            Log.e("inf 40" ,"Proximity  "+ valeur)
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    override fun onResume() {
        super.onResume()
        mySensors!!.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL)
        mySensors!!.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_FASTEST)
    }

    override fun onPause() {
        super.onPause()
        mySensors!!.unregisterListener(this)
    }
}
