package com.example.applicationformation1.kotlinFunctions

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class SimpleReceiver: BroadcastReceiver() {

    override fun onReceive(p0: Context?, intent: Intent?) {
        if(intent?.action ===  "com.example.mehdi.intent1"){
            Toast.makeText(p0, "Intent1", Toast.LENGTH_SHORT).show();
        }
        if(intent?.action ===  "com.example.mehdi.intent2"){
            Toast.makeText(p0, "Intent2", Toast.LENGTH_SHORT).show();
        }
    }
}