package com.example.applicationformation1.kotlinFunctions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.applicationformation1.R
import kotlinx.android.synthetic.main.activity_broadcas_reveiver.*

class BroadcasReveiver : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcas_reveiver)
        btn_broadcast_intent_1.setOnClickListener(this);
        btn_broadcast_intent_2.setOnClickListener(this);
        val intent = intent
        if (intent.getStringExtra("Message")!=null ){
            val message = intent.getStringExtra("Message");
            val title = intent.getStringExtra("title")+"On create"

            if (!message.isEmpty()){
                AlertDialog.Builder(this).setTitle(title)
                    .setMessage(message).setPositiveButton("OK", {dialogInterface, i ->  }).show()
            }

        }


    }

    override fun onResume() {
        super.onResume()
        //
        val intent = intent
        //Log.e("Onresume", intent.getStringExtra("message").toString());
        if (intent.getStringExtra("Message")!=null ){
            val message = intent.getStringExtra("Message").toString();
            val title = intent.getStringExtra("title").toString()+"On Resume"

            if (!message.isEmpty()){
                AlertDialog.Builder(this).setTitle(title)
                    .setMessage(message).setPositiveButton("OK", {dialogInterface, i ->  }).show()
            }

        }
    }

    fun brodcastIntent1(){
        var intent = Intent(this, SimpleReceiver::class.java);
        intent.action="com.example.mehdi.intent1"
        sendBroadcast(intent)
    }
    fun brodcastIntent2(){
        var intent = Intent(this, SimpleReceiver::class.java);
        intent.action="com.example.mehdi.intent2"
        sendBroadcast(intent)

    }
    override fun onClick(p0: View?) {
        when (p0?.id){
            btn_broadcast_intent_1.id -> {
                brodcastIntent1()
            }
            btn_broadcast_intent_2.id -> {
                brodcastIntent2()
            }
        }
    }
}
