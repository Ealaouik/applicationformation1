package com.example.applicationformation1.kotlinFunctions

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.MotionEventCompat
import com.example.applicationformation1.R
import kotlinx.android.synthetic.main.activity_gesture.*
import java.lang.Math.abs

class GestureActivity : AppCompatActivity(), GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener  {

    private var detectme :GestureDetectorCompat? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gesture)
        detectme= GestureDetectorCompat(this,this)
        detectme!!.setOnDoubleTapListener(this)
    }

    override fun onSingleTapConfirmed(p0: MotionEvent?): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = (existingText + "\n" + "singletap confirmed")
        return true
    }

    override fun onDoubleTap(p0: MotionEvent?): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n" + "onDoubleTap confirmed"
        return true
    }

    override fun onDoubleTapEvent(p0: MotionEvent?): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n"+"onDoubleTapEvent confirmed"
        return true
    }

    override fun onDown(p0: MotionEvent?): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n"+"onDown confirmed"
        return true
    }

    override fun onShowPress(p0: MotionEvent?) {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n"+ "onShowPress confirmed"
    }

    override fun onSingleTapUp(p0: MotionEvent?): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text =  existingText + "\n"+"onSingleTapUp confirmed"
        return true
    }

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n"+ "onScroll confirmed"
        return true
    }

    override fun onLongPress(p0: MotionEvent?) {
        var existingText = gestureStatusText.text.toString()
        gestureStatusText.text = existingText + "\n"+ "onLongPress confirmed"
    }

    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        var existingText = gestureStatusText.text.toString()
        if (abs((p0!!.x-p1!!.x)) > 50 ){
            gestureStatusText.text = existingText + "\n"+ "onFling X"
        }

        if(abs((p0!!.y-p1!!.y)) > 50 ){
            gestureStatusText.text = existingText + "\n"+ "onFling Y"
        }
        return true

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        this.detectme!!.onTouchEvent(event)
        return super.onTouchEvent(event)
    }
}
