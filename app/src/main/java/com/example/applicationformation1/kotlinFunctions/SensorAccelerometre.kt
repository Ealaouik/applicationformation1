package com.example.applicationformation1.kotlinFunctions

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import com.example.applicationformation1.GestionProduitActivity
import com.example.applicationformation1.MainActivity
import com.example.applicationformation1.R
import kotlinx.android.synthetic.main.activity_steps_sensor.*

class SensorAccelerometre : AppCompatActivity(), SensorEventListener {


    var accelerometreSensor : Sensor? =null

    var sensorManager : SensorManager?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sensor_accelerometre)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometreSensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager!!.registerListener(this, accelerometreSensor, SensorManager.SENSOR_DELAY_NORMAL)


        if(accelerometreSensor == null){

        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER){
            var xChanged = event!!.values[0].toString().toFloat();
            var yChanged = event!!.values[1].toString().toFloat();
            var zChanged = event!!.values[2].toString().toFloat();

            if (xChanged!=0f){
                val itent = Intent(this, GestionProduitActivity::class.java)
                startActivity(itent)

            }
            if (yChanged!=0f){
                val itent = Intent(this, MainActivity::class.java)
                startActivity(itent)
            }

        }
    }
}
