package com.example.applicationformation1.kotlinFunctions

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.content.getSystemService
import com.example.applicationformation1.R
import kotlinx.android.synthetic.main.activity_steps_sensor.*

class stepsSensor : AppCompatActivity(), SensorEventListener {

    var stepCounterSensor : Sensor? =null
    var sensorManager : SensorManager?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_steps_sensor)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        stepCounterSensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager!!.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_FASTEST)

    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_STEP_COUNTER){
            val valeur = event.values[0]
            Log.e("inf 40" ,"Counter  "+ valeur)
            nbrSteps.text=valeur.toString();
        }
    }

    override fun onRestart() {
        super.onRestart()
        sensorManager!!.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_FASTEST)

    }

    override fun onPause() {
        super.onPause()
        sensorManager!!.unregisterListener(this)
    }
}
