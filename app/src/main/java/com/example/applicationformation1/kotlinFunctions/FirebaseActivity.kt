package com.example.applicationformation1.kotlinFunctions

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.applicationformation1.data.game
import com.google.firebase.database.FirebaseDatabase




class FirebaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.applicationformation1.R.layout.activity_firebase)
        // Write a message to the database
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("message")
        var game = game("Nom", "2019" )
        myRef.push().setValue(game)

    }
}
