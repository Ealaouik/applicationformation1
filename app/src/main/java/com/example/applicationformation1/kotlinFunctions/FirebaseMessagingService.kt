package com.example.applicationformation1.kotlinFunctions

import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseMessagingTestService: FirebaseMessagingService() {


    var tag = "ServiceFireBase"
    override fun onMessageReceived(p0: RemoteMessage?) {
        Log.d(tag, "From "+p0?.from)
        Log.d(tag, "notificationBody "+p0?.notification?.body)

        val intent = Intent(this, BroadcasReveiver::class.java);
        intent.putExtra("Message",p0?.notification?.body )
        intent.putExtra("title", p0?.notification?.title)
        startActivity(intent)
    }
}