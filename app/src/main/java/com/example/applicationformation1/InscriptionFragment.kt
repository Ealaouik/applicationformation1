package com.example.applicationformation1


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.bumptech.glide.Glide
import com.example.applicationformation1.services.SharedPreference
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_inscription.*
import kotlinx.android.synthetic.main.fragment_inscription.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class InscriptionFragment : Fragment(), View.OnClickListener {

    var motDePasseIdentique = true;
    var PICK_IMAGE_REQUEST = 111;
    var bitmap: Bitmap? =null;
    var photo64:String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_inscription, container, false)
        view.bValiderInscription?.setOnClickListener(this)
        view.InstxtuserPhotocamera?.setOnClickListener(this)
        view.InstxtuserPhotofile?.setOnClickListener(this)


        /*view.ProfiletxtuserConfirmPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                var password = view.ProfiletxtuserPassword.text.toString();
                var confirmPassword = view.ProfiletxtuserConfirmPassword.text.toString()
                if (password.equals(confirmPassword)){
                    motDePasseIdentique = true;
                    view.ProfiletxtuserPassword.setBackgroundColor(Color.WHITE);
                    view.ProfiletxtuserConfirmPassword.setBackgroundColor(Color.WHITE);
                } else {
                    motDePasseIdentique = false;
                    view.ProfiletxtuserPassword.setBackgroundColor(Color.RED);
                    view.ProfiletxtuserConfirmPassword.setBackgroundColor(Color.RED);
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })*/

        remplirVille();

        //if we want to use client device we have to get the permission
        val permissions = arrayOf(
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.READ_PHONE_STATE",
            "android.permission.SYSTEM_ALERT_WINDOW",
            "android.permission.CAMERA",
            "android.permission.INTERNET"
        )
        //200 code demanding permission to use
        var requestCode = 200;
        requestPermissions(permissions, requestCode);
        return view
    }

    fun remplirVille(){
        var urlgetVilles = "http://nticformation.com/getListVille.php";
        val stringRequest =object : StringRequest(
            Request.Method.POST, urlgetVilles,
            Response.Listener<String> { response ->
                try {
                    val objarray = JSONArray(response)
                    var listVille=ArrayList<String>()
                    var obj=JSONObject()
                    for(i in 0..objarray.length()-1)
                    {
                        var villeobject: JSONObject = objarray.getJSONObject(i)
                        listVille.add(villeobject.get("nomville").toString())


                        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, listVille)
                        // Set layout to use when the list of choices appear
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        // Set Adapter to Spinner
                        InstxtuserVille.setAdapter(aa)

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()

                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                        .show()
                }
            } ) {
            override fun getParams(): MutableMap<String, String> {

                val params = HashMap<String, String>()
                val sharedPreference : SharedPreference = SharedPreference(activity!!.baseContext)

                return params
            }
        }
        VolleySingleton.instance?.addToRequestQueue(stringRequest, this.activity as AppCompatActivity)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bValiderInscription -> {
                //change le style du bouton
               // bValiderInscription?.setBackgroundResource(R.drawable.btnstyleactions)
                //si tous les champs sont remplis on envoie les données
                if(verifierInputs()) {
                    ajouterUser()
                }
                else
                {
                    Toast.makeText(activity, "Tout les champs sont obligatoire", Toast.LENGTH_LONG).show()

                }
            }
            //lorsque user clique sur camera icon on lance choseCamera pour prendre une photo
            R.id.InstxtuserPhotocamera -> run {
                //chosefile()
                choseCamera()
            }
            //when user chose a select file
            R.id.InstxtuserPhotofile -> run {
                chosefile()

            }

            else -> {
            }
        }
    }

    private fun ajouterUser() {
        val addurl = "http://nticformation.com/AddUser.php"
        //creating volley string request
        val stringRequest = object : StringRequest(
            Request.Method.POST, addurl,
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response)
                    Toast.makeText(activity, obj.getString("message"), Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()

                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" +volleyError.message, Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("nom", InstxtuserNom.text.toString())
                params.put("prenom", InstxtuserPrenom.text.toString())
                params.put("email", InstxtuserEmail.text.toString())
                params.put("tel", InstxtuserTel.text.toString())
                params.put("ville",InstxtuserVille.selectedItem.toString())
                params.put("login", InstxtuserLogin.text.toString())
                params.put("password", InstxtuserPassword.text.toString())
                //photo is send in 64 format
                params.put("photo64",photo64)
                if(InstxtuserF.isChecked==true) { params.put("sexe", "F")      }
                else  { params.put("sexe", "M")     }
                return params
            }
        }
        //adding request to queue
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }
    fun chosefile()
    {
        //Intent permet de lancer une activity externe (implicit) or internet (implicit)
        val intent = Intent()
        intent.type = "image/*"  //image default pic directory in the device
        intent.action = Intent.ACTION_PICK //chemin du pic folder in the device
        //this function will show filechoser and when user select a file ,it will keep the selected file
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST)

    }
    fun choseCamera()
    {
        //launch camera intent
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        //get the pic from camera
        startActivityForResult(intent, PICK_IMAGE_REQUEST)

    }

    //this function will be executed when ,we take a pic or chose a file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //if there no problem  we get the file path of the pic
        if (resultCode != Activity.RESULT_OK) return
        val filePath = data?.data
        try {
            //getting image from gallery
            bitmap = MediaStore.Images.Media.getBitmap(activity?.getContentResolver(), filePath)

            //Setting image to ImageView
            InstxtuserPhoto.setImageBitmap(bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //converting image to base64 string
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, baos)
        val imageBytes = baos.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        photo64=imageString
        Toast.makeText(activity,"file is chosen",Toast.LENGTH_SHORT).show()
    }
    fun verifierInputs():Boolean
    {
        if(InstxtuserNom.text.toString().equals("")) return false
        if(InstxtuserPrenom.text.toString().equals("")) return false
        if(InstxtuserEmail.text.toString().equals("")) return false
        if(InstxtuserLogin.text.toString().equals("")) return false
        if(InstxtuserPrenom.text.toString().equals("")) return false
        if(InstxtuserConfirmPassword.text.toString().equals("")) return false
        if(!motDePasseIdentique) return  false
        return true

    }

}
