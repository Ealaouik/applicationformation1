package com.example.applicationformation1


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_ajouter_produit.*
import kotlinx.android.synthetic.main.fragment_ajouter_produit.view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream

/**
 * A simple [Fragment] subclass.
 */
class AjouterProduit : Fragment() , View.OnClickListener {

    var PICK_IMAGE_REQUEST = 222
    var bitmap: Bitmap? = null
    //used to store image 64
    var  photo64:String="";
    //to save a list of images
    var photo64s=arrayListOf("v","v","v")
    var positionImage=0 //the remember the position of chosen pic

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_ajouter_produit, container, false)
        view.produitFile.setOnClickListener(this);
        view.produitCamera.setOnClickListener(this);
        view.produitbValider.setOnClickListener(this);

        var listCategories= listOf("Ordinateur","Téléphone","TV","auto")
        val adapterCategory = ArrayAdapter(activity, android.R.layout.simple_spinner_item, listCategories)

        view.marqueProduit.setAdapter(adapterCategory)

        view.ajoutProduitImage1.setOnClickListener(this);
        view.ajoutProduitImage2.setOnClickListener(this);
        view.ajoutProduitImage3.setOnClickListener(this);

        view.ajoutProduitImage1.setImageResource(R.drawable.imagevide)
        view.ajoutProduitImage2.setImageResource(R.drawable.imagevide)
        view.ajoutProduitImage3.setImageResource(R.drawable.imagevide)

        //if we want to use client device we have to get the permission
        val permissions = arrayOf(
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.READ_PHONE_STATE",
            "android.permission.SYSTEM_ALERT_WINDOW",
            "android.permission.CAMERA"
        )
        //200 code demanding permission to use
        var requestCode = 200;
        requestPermissions(permissions, requestCode);

        return view
    }


    override fun onClick(p0: View?) {
        when(p0?.id){

            produitFile.id -> {
                chosefile();
            }
            produitCamera.id -> {
                choseCamera()
            }

            produitbValider.id -> {
                if (!checkproduitIsEmpty()) {
                    AjouterProduit()
                } else {
                    Toast.makeText(
                        activity,
                        "Tous les champs sont obligatoires",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
            //if you want ot modifie the pics
                //
            ajoutProduitImage1.id -> {
                ajoutProduitImage1.setImageResource(R.drawable.imagevide)
                positionImage=0;
                photo64s?.set(positionImage,"v")
            }

            ajoutProduitImage2.id -> {
                ajoutProduitImage2.setImageResource(R.drawable.imagevide)
                positionImage=1;
                photo64s?.set(positionImage,"v")
            }

            ajoutProduitImage3.id -> {
                ajoutProduitImage3.setImageResource(R.drawable.imagevide)
                positionImage=2;
                photo64s?.set(positionImage,"v")
            }

        }
    }
    //2check if all input are have a value
    fun checkproduitIsEmpty():Boolean
    {
        if(codeProduit.text.toString().equals("")) return true
        if(libelleProduit.text.toString().equals("")) return true
        if(descriptionProduit.text.toString().equals("")) return true
        if(prixProduit.text.toString().equals("")) return true
        if(prixVenteProduit.text.toString().equals("")) return true
        if(marqueProduit.selectedItem.toString().equals("")) return true
        return false
    }
    fun chosefile()
    {
        //Intent permet de lancer une activity externe (implicit) or internet (implicit)
        val intent = Intent()
        intent.type = "image/*"  //image default pic directory in the device
        intent.action = Intent.ACTION_PICK //chemin du pic folder in the device
        //this function will show filechoser and when user select a file ,it will keep the selected file
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST)

    }
    fun choseCamera()
    {
        //launch camera intent
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        //get the pic from camera
        startActivityForResult(intent, PICK_IMAGE_REQUEST)

    }

    private fun AjouterProduit() {
        val addurl = "http://nticformation.com/AddProduct.php"
        //creating volley string request
        val stringRequest = object : StringRequest(
            Request.Method.POST, addurl,
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response)
                    Toast.makeText(activity, obj.getString("message"), Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()

                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" +volleyError.message, Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("libelle", libelleProduit.text.toString())
                params.put("description", descriptionProduit.text.toString())
                params.put("prix", prixProduit.text.toString())
                params.put("prixVente", prixVenteProduit.text.toString())
                params.put("marque",marqueProduit.selectedItem.toString())
                params.put("code", codeProduit.text.toString())
                params.put("qtestock", quantiteProduit.text.toString())
                //photo is send in 64 format
                params.put("photo64_0",photo64s[0])
                params.put("photo64_1",photo64s[1])
                params.put("photo64_2",photo64s[2])


                for(i in 0..2)
                {
                    Log.e("iii= $i", photo64s.get(i).toString())
                }

                return params
            }
        }
        //adding request to queue
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }


    //this function will be executed when ,we take a pic or chose a file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //if there no problem  we get the file path of the pic

        if (resultCode != Activity.RESULT_OK) return
        val filePath = data?.data
        try {
            //getting image from gallery
            bitmap = MediaStore.Images.Media.getBitmap(activity?.getContentResolver(), filePath)
            if(positionImage==0) {
                ajoutProduitImage1.setImageBitmap(bitmap)
            }
            if(positionImage==1) {
                ajoutProduitImage2.setImageBitmap(bitmap)
            }
            if(positionImage==2) {
                ajoutProduitImage3.setImageBitmap(bitmap)
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
        //converting image to base64 string
        var baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, baos)
        var imageBytes = baos.toByteArray()
        var imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)


        if (photo64s?.get(positionImage) == "v") {
            photo64s?.set(positionImage,imageString)

            if(positionImage<3) {
                positionImage++
            }
            Log.e("show",imageString)

        }
    }

}
