package com.example.applicationformation1


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.example.applicationformation1.adapters.UserAdapter
import com.example.applicationformation1.data.User
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_list_users.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class ListUsersFragment(var actiCaller : MainActivity) : Fragment() {

    var page = 0;
    var recyclerView : RecyclerView?=null;
    var userList = ArrayList<User>();
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_users, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView = view?.findViewById(R.id.listUsersRecyclerView);

        recyclerView?.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy<0){
                    Toast.makeText(activity, "You are scrolling up", Toast.LENGTH_SHORT).show()
                }
                else if (dy>0) {
                    if (page <1 ) {
                        page += 7
                        getListUsers()
                    }
                }
            }
        });
        recyclerView?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        getListUsers()
    }
    fun getListUsers() {
        listUsersProgressbar.visibility = View.VISIBLE;
        var listUserUrl = "http://nticformation.com/listUser.php";
        val stringRequest =object : StringRequest(
            Request.Method.POST, listUserUrl,
            Response.Listener<String> { response ->
                try {
                    val objarray = JSONArray(response)
                    for (i in 0..objarray.length() - 1) {
                        var user= User()
                        var userObject: JSONObject = objarray.getJSONObject(i)
                        //libelle,prix,marque,prixvente,qteStock,image,imagelist,description

                        user.nom = userObject.getString("nom").toString();
                        user.prenom = userObject.getString("prenom").toString();
                        user.email = userObject.getString("email").toString();
                        user.tel = userObject.getString("tel").toString();
                        user.sexe = userObject.getString("sexe").toString().toCharArray()[0];
                        user.ville = userObject.getString("ville").toString();
                        user.photo = userObject.getString("photo").toString();
                        user.login = userObject.getString("login").toString();
                        user.id = userObject.getString("id").toString().toInt();
                        userList.add(user)
                    }

                    val adapter = UserAdapter(this,userList,actiCaller );
                    //now adding the adapter to recyclerview
                    recyclerView?.setAdapter(adapter)
                    //when we update recylerview or produitList we notify the adapter
                    adapter?.notifyDataSetChanged();
                    listUsersProgressbar.visibility=View.GONE

                } catch (e: JSONException){
                    e.printStackTrace();
                    Toast.makeText(activity, "Probleme de données", Toast.LENGTH_SHORT).show()
                    listUsersProgressbar.visibility=View.GONE
                }
            }, object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                        .show()
                }
            } ) {
            override fun getParams(): MutableMap<String, String> {

                val params = HashMap<String, String>()
                params.put("page", ""+page)

                return params
            }
        }
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }
}
