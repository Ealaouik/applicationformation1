package com.example.applicationformation1.services

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley


class VolleySingleton : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T> )  {
            request.tag = TAG
            requestQueue?.add(request)
    }
    fun <T> addToRequestQueue(request: Request<T> , appCompatActivity: AppCompatActivity)  {
        if (isConnected()){
            request.tag = TAG
            requestQueue?.add(request)
        }
        else {
            Toast.makeText(appCompatActivity, "Veuillez vous connecter", Toast.LENGTH_SHORT).show();
        }

    }

    companion object {
        private val TAG = VolleySingleton::class.java.simpleName
        @get:Synchronized var instance: VolleySingleton? = null
            private set

    }


    fun isConnected (): Boolean{
        val connectivityManager: ConnectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        Log.e("network info ", networkInfo.toString());
        val result = (networkInfo !=null) && networkInfo.isConnected ;
        return result
    }
}