package com.example.applicationformation1


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.bumptech.glide.Glide
import com.example.applicationformation1.services.SharedPreference
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_inscription.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment(), View.OnClickListener {

    var sharedPreference : SharedPreference?= null;
    var bitmap: Bitmap? =null;
    var PICK_IMAGE_REQUEST = 112;
    var photo64:String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_profile, container, false);
        sharedPreference= SharedPreference(activity!!.baseContext)
        var id =sharedPreference?.getValueString("iduser").toString()
        if (id != null){
            getUserProfile()
            (activity as AppCompatActivity).supportActionBar?.show()
        }

        view.bValiderProfilecription.setOnClickListener(this);
        return view
    }

    fun getUserProfile(){
        val login = sharedPreference?.getValueString("login")
        if (login == null ){
            Toast.makeText(activity, "L'utilisateur n'est pas connecté", Toast.LENGTH_LONG).show()

        } else {

            var urlgetUser = "http://nticformation.com/profile.php";


            val stringRequest =object : StringRequest(
                Request.Method.POST, urlgetUser,
                Response.Listener<String> { response ->  
                    try {
                        val obj = JSONObject(response);
                        ProfiletxtuserNom.setText(obj.getString("nom").toString())
                        ProfiletxtuserEmail.setText(obj.getString("email").toString())
                        ProfiletxtuserPrenom.setText(obj.getString("prenom").toString())
                        ProfiletxtuserLogin.setText(obj.getString("login").toString())
                        ProfiletxtuserVille.setText(obj.getString("ville").toString())
                        ProfiletxtuserTel.setText(obj.getString("tel").toString())
                        ProfiletxtuserLogin.setText(obj.getString("login").toString())
                        ProfiletxtuserPassword.setText(obj.getString("password").toString())

                        var photoUrl = "http://nticformation.com/"+obj.getString("photo").toString()
                        Glide.with(this).load(photoUrl).into(ProfiletxtuserPhoto)

                        if(obj.getString("sexe").toString().equals("M"))
                        {
                            ProfiletxtuserM.isChecked=true;
                        }
                        else
                        {
                            ProfiletxtuserF.isChecked=true;
                        }

                    } catch (e: JSONException){
                        e.printStackTrace();
                        Toast.makeText(activity, "Probleme de données", Toast.LENGTH_SHORT).show()
                    }
                }, object : Response.ErrorListener {
                    override fun onErrorResponse(volleyError: VolleyError) {
                        Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                            .show()
                    }
                } ) {
                override fun getParams(): MutableMap<String, String> {

                    val params = HashMap<String, String>()
                    val sharedPreference : SharedPreference = SharedPreference(activity!!.baseContext)
                    var id =sharedPreference?.getValueString("iduser").toString()

                    params.put("iduser",id )
                    return params
                }
            }
            VolleySingleton.instance?.addToRequestQueue(stringRequest)
        }
    }

    private fun updateUser() {
        val addurl = "http://nticformation.com/UpdateUser.php"
        //creating volley string request
        val stringRequest = object : StringRequest(
            Request.Method.POST, addurl,
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response)
                    Toast.makeText(activity, obj.getString("message"), Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()

                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" +volleyError.message, Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                val id = sharedPreference!!.getValueString("iduser").toString();
                params.put("nom", id)
                params.put("prenom", ProfiletxtuserPrenom.text.toString())
                params.put("email", ProfiletxtuserEmail.text.toString())
                params.put("tel", ProfiletxtuserTel.text.toString())
                params.put("ville",ProfiletxtuserVille.toString())
                params.put("login", ProfiletxtuserLogin.text.toString())
                params.put("password", ProfiletxtuserPassword.text.toString())
                //photo is send in 64 format
                params.put("photo64",photo64)
                if(ProfiletxtuserF.isChecked==true) { params.put("sexe", "F")      }
                else  { params.put("sexe", "M")     }
                return params
            }
        }
        //adding request to queue
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }


    fun chosefile()
    {
        //Intent permet de lancer une activity externe (implicit) or internet (implicit)
        val intent = Intent()
        intent.type = "image/*"  //image default pic directory in the device
        intent.action = Intent.ACTION_PICK //chemin du pic folder in the device
        //this function will show filechoser and when user select a file ,it will keep the selected file
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST)

    }
    fun choseCamera()
    {
        //launch camera intent
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        //get the pic from camera
        startActivityForResult(intent, PICK_IMAGE_REQUEST)

    }

    //this function will be executed when ,we take a pic or chose a file
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //if there no problem  we get the file path of the pic
        if (resultCode != Activity.RESULT_OK) return
        val filePath = data?.data
        try {
            //getting image from gallery
            bitmap = MediaStore.Images.Media.getBitmap(activity?.getContentResolver(), filePath)

            //Setting image to ImageView
            InstxtuserPhoto.setImageBitmap(bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //converting image to base64 string
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, baos)
        val imageBytes = baos.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        photo64=imageString
        Toast.makeText(activity,"file is chosen",Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bValiderProfilecription -> {
                //change le style du bouton
                // bValiderInscription?.setBackgroundResource(R.drawable.btnstyleactions)
                //si tous les champs sont remplis on envoie les données
                if(verifierInputs()) {
                    updateUser()
                }
                else
                {
                    Toast.makeText(activity, "Tout les champs sont obligatoire", Toast.LENGTH_LONG).show()

                }
            }
            //lorsque user clique sur camera icon on lance choseCamera pour prendre une photo
            R.id.ProfiletxtuserPhotocamera -> run {
                //chosefile()
                choseCamera()
            }
            //when user chose a select file
            R.id.ProfiletxtuserPhotofile -> run {
                chosefile()

            }

            else -> {
            }
        }
    }

    fun verifierInputs():Boolean
    {
        if(ProfiletxtuserNom.text.toString().equals("")) return false
        if(ProfiletxtuserPrenom.text.toString().equals("")) return false
        if(ProfiletxtuserEmail.text.toString().equals("")) return false
        if(ProfiletxtuserLogin.text.toString().equals("")) return false
        if(ProfiletxtuserPrenom.text.toString().equals("")) return false
        return true

    }
}
