package com.example.applicationformation1

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import com.example.applicationformation1.services.SharedPreference
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener  {

    var sharedPreference: SharedPreference ?= null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btabconnexion.setOnClickListener(this);
        btabIncription.setOnClickListener(this);
        sharedPreference = SharedPreference(this)


        if(sharedPreference?.getValueString("login") == null){
            this.supportActionBar?.hide()
            btabIncription.setBackgroundResource(R.drawable.btnstyinactif);
            btabconnexion.setBackgroundResource(R.drawable.btnsty);
            var fragmentConnexion=ConnexionFragment()
            supportFragmentManager
                // 3
                .beginTransaction()
                // 4
                .replace(R.id.FragmentConnexionIns, fragmentConnexion, "Connexion")
                // 5
                .commit()

        }else {
            this.supportActionBar?.show()
            var fragmentProfile=ProfileFragment()
            supportFragmentManager
                // 3
                .beginTransaction()
                // 4
                .replace(R.id.FragmentConnexionIns, fragmentProfile, "Connexion")
                // 5
                .commit()
            btabIncription.setBackgroundResource(R.drawable.btnstyinactif);
            btabconnexion.setBackgroundResource(R.drawable.btnsty);
            btabconnexion.setText("Profile")
        }


    }

    override fun onRestart() {
        super.onRestart()

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        var orientation = getScreenOrientation(this);

        if (orientation == "SCREEN_ORIENTATION_LANDSCAPE" || orientation == "SCREEN_ORIENTATION_REVERSE_LANDSCAPE"){
            btabconnexion.setTextColor(Color.RED);
        }
        else {
            btabconnexion.setTextColor(Color.BLUE);

        }
    }
    override fun onClick(v: View?) {

        if(btabconnexion.id == v?.id){
            var login = sharedPreference?.getValueString("login");
            if (login==null){
                var fragmentConnexion=ConnexionFragment()
                supportFragmentManager
                    // 3
                    .beginTransaction()
                    // 4
                    .replace(R.id.FragmentConnexionIns, fragmentConnexion, "Connexion")
                    // 5
                    .commit()
                btabIncription.setBackgroundResource(R.drawable.btnstyinactif);
                btabconnexion.setBackgroundResource(R.drawable.btnsty);
                btabconnexion.setText("Connexion")
            }
            else {
                var fragmentProfile=ProfileFragment()
                supportFragmentManager
                    // 3
                    .beginTransaction()
                    // 4
                    .replace(R.id.FragmentConnexionIns, fragmentProfile, "Connexion")
                    // 5
                    .commit()
                btabIncription.setBackgroundResource(R.drawable.btnstyinactif);
                btabconnexion.setBackgroundResource(R.drawable.btnsty);
                btabconnexion.setText("Profile")
            }

        } else if (btabIncription.id == v?.id){
            var inscriptionConnexion=InscriptionFragment()
            supportFragmentManager
                // 3
                .beginTransaction()
                // 4
                .replace(R.id.FragmentConnexionIns, inscriptionConnexion, "Inscription")
                // 5
                .commit()

            btabIncription.setBackgroundResource(R.drawable.btnsty);
            btabconnexion.setBackgroundResource(R.drawable.btnstyinactif);
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.principalactionmenu, menu )


        sharedPreference = SharedPreference(this.applicationContext)
        if(sharedPreference?.getValueString("login") != null){
            menu?.add("welcome "+ sharedPreference?.getValueString("login"))
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val idMenu = item?.itemId;

        if (idMenu == R.id.gestion_produit){
            val itent = Intent(this, GestionProduitActivity::class.java)
            startActivity(itent)
            return true
        }
        else if (idMenu == R.id.gestion_client){
            var userFragment = ListUsersFragment(this)
            supportFragmentManager.beginTransaction().add(
                R.id.FragmentConnexionIns, userFragment, "connexion"
            ).commit()
            return true
        }
        else if (idMenu == R.id.action_logout){
            this.supportActionBar?.hide()
            var connexionFragment = ConnexionFragment()
            supportFragmentManager.beginTransaction().add(
                R.id.FragmentConnexionIns, connexionFragment, "connexion"
            ).commit()
            sharedPreference!!.clearSharedPreference()

            return true
        }

        return super.onOptionsItemSelected(item)
    }


    fun getScreenOrientation(context: Context): String {
        val screenOrientation =
            (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.orientation
        when (screenOrientation) {
            Surface.ROTATION_0 -> return "SCREEN_ORIENTATION_PORTRAIT"
            Surface.ROTATION_90 -> return "SCREEN_ORIENTATION_LANDSCAPE"
            Surface.ROTATION_180 -> return "SCREEN_ORIENTATION_REVERSE_PORTRAIT"
            else -> return "SCREEN_ORIENTATION_REVERSE_LANDSCAPE"
        }
    }
}
