package com.example.applicationformation1


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.bumptech.glide.Glide
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.fragment_detail_product.*
import kotlinx.android.synthetic.main.fragment_detail_product.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class DetailProductFragment : Fragment(), View.OnClickListener {

    var idprod : String? = null ;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var args: Bundle? = getArguments();

        var view =  inflater.inflate(R.layout.fragment_detail_product, container, false)

        idprod = args?.getString("idproduct")
        Log.e("Message", idprod);
        getProductDetails();
        view.shareProduct.setOnClickListener(this);
        return view;
    }
    fun getProductDetails(){

        val detailProductUrl = "http://nticformation.com/DetailProduct.php"


        val stringRequest = object : StringRequest(
            Request.Method.POST, detailProductUrl,
            Response.Listener<String> { response ->
                try {
                    //libelle,prix,marque,prixvente,qteStock,image,imagelist,description
                    val objarray = JSONArray(response)

                    var obj: JSONObject = objarray.getJSONObject(0)
                    detailproLibelle.setText(obj.getString("libelle").toString())
                    //modify the title of the nav bar
                    activity!!.title="Info about :"+obj.getString("libelle").toString()
                    detailproPrix.setText(obj.getString("prix").toString()+" Dhs")
                    detailpromarque.setText(obj.getString("marque").toString())
                    detailproprixVente.setText(obj.getString("prixvente").toString()+" Dhs")
                    detailprostock.setText(obj.getString("qteStock").toString())
                    detailprocode.setText(obj.getString("code").toString())
                    detailprodescription.setText(obj.getString("description").toString())
                    var listeDiaporama=obj.getString("imagelist").toString()
                    var photoUrl="http://nticformation.com/"+obj.getString("image").toString()
                    //this library used to load picture into the imageView
                    Glide.with(this).load(photoUrl).into(detailproPhoto)
                    //because the listimage send like this image1,image2,...
                    //split convert this list to array
                    var ArraylisteDiaporama: List<String> = listeDiaporama.split(",")
                    //used to manage the diaporama imageview
                    var diaporamaImageview=ArrayList<ImageView>()
                    diaporamaImageview.add(deitailprocpicDiaporama1)
                    diaporamaImageview.add(detailpropicDiaporama2)
                    diaporamaImageview.add(detailpropicDiaporama3)
                    diaporamaImageview.add(detailpropicDiaporama4)
                    for(diap in diaporamaImageview)
                    {
                        diap.visibility=View.GONE
                    }
                    var position=0 //used to populate the imageView from ArayListDiaporama
                    for( imagelink in ArraylisteDiaporama)
                    {
                        var photoUrl="http://nticformation.com/"+imagelink.toString()
                        //this library used to load picture into the imageView
                        Glide.with(this).load(photoUrl).into(diaporamaImageview.get(position))
                        diaporamaImageview.get(position).visibility=View.VISIBLE
                        if(position<3) position++

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                        .show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params.put("idproduct", ""+idprod)
                Log.e("idprod",""+idprod)
                return params
            }
        }
        //adding request to queue
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }

    override fun onClick(v: View?) {
        if(v?.id == shareProduct.id){
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type= "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My subject")
            val app_url = "L'URL passé"
            shareIntent.putExtra(Intent.EXTRA_TEXT, app_url)
            startActivity(Intent.createChooser(shareIntent,"Share via"))

        }
    }

}
