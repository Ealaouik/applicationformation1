package com.example.applicationformation1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ShareActionProvider
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.applicationformation1.services.SharedPreference
import com.google.android.material.navigation.NavigationView

class GestionProduitActivity : AppCompatActivity() {

    var sharedPreference: SharedPreference ?= null;

    private lateinit var mDrawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreference = SharedPreference(this)
        if (sharedPreference?.getValueString("login") == null){
            val itent = Intent(this, MainActivity::class.java)
            startActivity(itent)

        }

        setContentView(R.layout.activity_gestion_produit)
        //get the created toolBar
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        //set toolbar to the activity
        setSupportActionBar(toolbar)
        //set icon menu
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            //display icon menu
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.menuicon)
        }

        //get the drawer menu
        mDrawerLayout = findViewById(R.id.drawer_layout)

        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()

            // Handle navigation view item clicks here.
            when (menuItem.itemId) {

                R.id.navAjouterProduit -> {
                   // Toast.makeText(this, "Ajouter",Toast.LENGTH_SHORT).show();
                    var ajouterProduitFragment=AjouterProduit()
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .replace(R.id.gestionProduitframe, ajouterProduitFragment, "Ajouter un produit")
                        // 5
                        .commit()
                }
                R.id.navListProduit -> {
                   // var listProduit_Fragment=ListProduit_Fragment()
                    var connexionFragment=ListProduit(this)
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .replace(R.id.gestionProduitframe, connexionFragment, "Liste produit")
                        // 5
                        .commit()
                    Toast.makeText(this, "List",Toast.LENGTH_SHORT).show();
                }
                R.id.navRechercherProduit -> {
                /* var listProduit_Fragment=ListProduit_Fragment()
                 supportFragmentManager
                     // 3
                     .beginTransaction()
                     // 4
                     .replace(R.id.gestionProduitframe, listProduit_Fragment, "Liste produit")
                     // 5
                     .commit()*/
                Toast.makeText(this, "Recherche", Toast.LENGTH_SHORT).show();
            }
                R.id.nav_VendreProduit -> {
                    /* var venteFragment=VenteFragment()
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .replace(R.id.gestionProduitframe, venteFragment, "Vendre produit")
                        // 5
                        .commit()                    */
                    Toast.makeText(this, "Vendre", Toast.LENGTH_SHORT).show();
                }

                R.id.nav_deconnexionUUser -> {
                    sharedPreference!!.clearSharedPreference()
                    val itent = Intent(this, MainActivity::class.java)
                    startActivity(itent)
                }

            }
            // Add code here to update the UI based on the item selected


            true
        }

    }

    //appbar - toolbar button click
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }
}
