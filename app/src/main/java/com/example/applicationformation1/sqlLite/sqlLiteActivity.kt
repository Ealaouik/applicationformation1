package com.example.applicationformation1.sqlLite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.applicationformation1.R
import com.example.applicationformation1.data.UsersSqlLite
import kotlinx.android.synthetic.main.activity_sql_lite.*

class sqlLiteActivity : AppCompatActivity() {
    lateinit var usersDBHelper : UsersDBHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sql_lite)
        usersDBHelper = UsersDBHelper(this)
        usersDBHelper.insertUser(UsersSqlLite("MehdiID", 28, "Mehdi"))
        for (i in 0..10){
            usersDBHelper.insertUser(UsersSqlLite("testID"+i, 20+i, "test"+i))

        }

        var listusers = usersDBHelper.selectAllUsers().toString()
        Toast.makeText(this,listusers,Toast.LENGTH_SHORT ).show();

        listUsersSqlLite.text = listusers;
    }
}
