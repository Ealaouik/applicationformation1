package com.example.applicationformation1


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.example.applicationformation1.services.SharedPreference
import com.example.applicationformation1.services.VolleySingleton
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_connexion.*
import kotlinx.android.synthetic.main.fragment_connexion.view.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class ConnexionFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_connexion, container, false)
        view.conbValiderConnexion.setOnClickListener(this)



        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onClick(v: View?) {
        if (conbValiderConnexion.id == v?.id){

            var login = contxtuserLogin.text.toString();
            var password = contxtuserPassword.text.toString();

            if (login.isEmpty() || password.isEmpty()){
                Toast.makeText(activity,"Veuillez saisir tous les chmaps", Toast.LENGTH_SHORT).show()
            }
            else {
                connectUser(login,password);
            }
        }


    }

    private fun connectUser(login:String, password :String){
        val permissions : Array<String> = arrayOf(
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        )
        var requestCode =200;
        requestPermissions(permissions,requestCode);
        val sharedPreference : SharedPreference = SharedPreference(activity!!.baseContext);
        var urlAddUser = "http://nticformation.com/ConnexionUser.php";

        var connecte : Boolean = VolleySingleton.instance!!.isConnected()

        if (connecte){
            val stringRequest =object : StringRequest(
                Request.Method.POST, urlAddUser,
                Response.Listener<String> { response ->
                    try {
                        val obj = JSONObject(response)

                        if (obj.getString("error").equals("false")){
                            sharedPreference.save("login", obj.getString("message"))
                            sharedPreference.save("iduser", obj.getString("iduser"))
                            sharedPreference.save("email", obj.getString("email"))
                            Toast.makeText(activity, obj.getString("message"), Toast.LENGTH_LONG).show()

                            var userProfileFrag=ProfileFragment()
                            activity!!.supportFragmentManager

                                .beginTransaction()

                                .add(R.id.FragmentConnexionIns, userProfileFrag, "Connexion")

                                .commit()
                            activity!!.btabconnexion.setText("Profile")

                        } else {
                            val message = obj.getString("message")
                            Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
                            val alertBuilder = AlertDialog.Builder(activity);
                            alertBuilder.setTitle(message);
                            alertBuilder.setMessage("Veuillez choisir une action")
                            alertBuilder.setIcon(android.R.drawable.ic_dialog_alert)
                            alertBuilder.setNegativeButton ("Cancel"){
                                x , i ->
                                Toast.makeText(activity, "Connexion canceled", Toast.LENGTH_SHORT).show()
                            }

                            alertBuilder.setPositiveButton("inscription") {
                                dialogInterface, i ->
                                val inscriptionFragment = InscriptionFragment();
                                activity!!.supportFragmentManager

                                    .beginTransaction()

                                    .add(R.id.FragmentConnexionIns, inscriptionFragment, "Connexion")

                                    .commit()
                                activity!!.btabIncription.setBackgroundResource(R.drawable.btnstyinactif);
                                activity!!.btabconnexion.setBackgroundResource(R.drawable.btnsty);

                            }
                            val alertDialog : AlertDialog = alertBuilder.create()
                            alertDialog.show()


                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(activity, "JsonError", Toast.LENGTH_LONG).show()

                    }
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(volleyError: VolleyError) {
                        Toast.makeText(activity, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                            .show()
                    }
                }) {
                override fun getParams(): MutableMap<String, String> {
                    val params = HashMap<String, String>()
                    params.put("email",login )
                    params.put("password",password )
                    return params
                }
            }
            VolleySingleton.instance?.addToRequestQueue(stringRequest)
        }
        else {
            Toast.makeText(activity, "Veuillez vous connecter", Toast.LENGTH_SHORT).show()
        }
    }
}
