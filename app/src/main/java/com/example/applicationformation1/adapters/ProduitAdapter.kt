package com.example.applicationformation1.adapters

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.bumptech.glide.Glide
import com.example.applicationformation1.DetailProductFragment
import com.example.applicationformation1.GestionProduitActivity
import com.example.applicationformation1.ListProduit
import com.example.applicationformation1.R
import com.example.applicationformation1.data.Produit
import com.example.applicationformation1.services.VolleySingleton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ProduitAdapter (val activity: ListProduit,
                      private val ProduitList: List<Produit>,
                      var activityCaller: GestionProduitActivity) :
    RecyclerView.Adapter<ProduitAdapter.ProduitLine>(){
    //used to save the selected card for deleting
    val cardSelecteds=ArrayList<CardView>()

    inner class ProduitLine(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        //the card properties
        var libelle: TextView
        var marque: TextView
        var prix: TextView
        var image: ImageView
        var code:TextView
        var stock:TextView
        var selectitem: CheckBox
        var mCardView:CardView
        var floatAction : FloatingActionButton?
        init{
            libelle = view.findViewById(R.id.listproLibelle)
            marque = view.findViewById(R.id.listproMarque)
            prix = view.findViewById(R.id.listproPrix)
            image=view.findViewById(R.id.listproPhoto)
            code=view.findViewById(R.id.listproCode)
            selectitem=view.findViewById(R.id.listprocheck)
            mCardView =  view.findViewById(R.id.listeprocard);
            stock=view.findViewById(R.id.listproStock)
            floatAction=activity.view?.findViewById(R.id.listprodActions)
            mCardView?.setOnClickListener(this);
            selectitem.setOnClickListener(this)
            floatAction?.setOnClickListener(this)
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id){
                floatAction?.id -> {
                    for (card in cardSelecteds) {
                        card.removeAllViews()
                    }
                    //this function call a webservice for deleting a list of selected product
                    removeProducts(cardSelecteds, v)
                }

                //we click on checkBox we add the selected card to the list in order
                //to remove them from the list and the datababse
                selectitem.id -> {

                    //if the card is selected so the next clique will delected the card
                    if (selectitem.isChecked == false) {
                        mCardView.setCardBackgroundColor(Color.WHITE)
                        cardSelecteds.remove(mCardView)


                    }
                    //if the card is selected ,we added to the cardSelecteds list
                    else {
                        mCardView.setCardBackgroundColor(Color.YELLOW)
                        cardSelecteds.add(mCardView)
                    }
                }
                 mCardView.id -> {

                    val detailproduitf = DetailProductFragment()
                    val args = Bundle()
                    //we save id of actual product in the tag of each card
                    args.putString("idproduct", mCardView.tag.toString())
                    detailproduitf.setArguments(args)
                    //launch the detail activity
                    activityCaller.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.gestionProduitframe, detailproduitf, "detailsproduct")
                        .commit()

                }
            }
        }
    }

    //this function will be execute in order to make the holder view (Card)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProduitLine {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_produit_row, parent, false)
        return ProduitLine(itemView)
    }

    //    //for each object(product) on the list this function will map the data to the graphic objects
    override fun onBindViewHolder(holder: ProduitLine, position: Int) {
        val produit:Produit = ProduitList[position]
        var photoUrl="http://nticformation.com/"+produit.image
        Glide.with(activity).load(photoUrl).into(holder.image)
        holder.libelle.text="Libelle :"+produit.libelle
        holder.marque.text="Marque :"+produit.marque
        holder.prix.text="Prix :"+produit.prix+" dhs"
        holder.code.text="Code :"+produit.code
        holder.stock.text="Stock "+produit.stock
        //will used to delete or view the product
        holder.mCardView.setTag(produit.id);
    }
    //get the count of the produitList in the recylerView
    override fun getItemCount(): Int {
        return ProduitList.size
    }


    private fun removeProducts(cardSelecteds: ArrayList<CardView>,v:View?) {
        //get all id , make a json array and send it to webservice
        //[{"id":"25"},{"id":"24"},{"id":"23"},{"id":"22"}]
        var tob:JSONArray= JSONArray()
        for(i in 0..cardSelecteds.size-1) {
            var ob:JSONObject=JSONObject()
            ob.put("id",cardSelecteds.get(i).tag.toString())

            tob.put(ob)
        }
        val addurl = "http://nticformation.com/deleteproduit.php"
        //creating volley string request :w
        val stringRequest = object : StringRequest(
            Request.Method.POST, addurl,
            Response.Listener<String> { response ->
                try {
                    val objarray = JSONArray(response)
                    var obj: JSONObject = objarray.getJSONObject(0)
                    Toast.makeText(v?.context, obj.getString("message").toString(), Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Toast.makeText(v?.context, "JsonError", Toast.LENGTH_LONG).show()

                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    Toast.makeText(v?.context, "Error2" + volleyError.message, Toast.LENGTH_LONG)
                        .show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("idproduct",tob.toString())

                return params
            }
        }
        //adding request to queue
        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }
}