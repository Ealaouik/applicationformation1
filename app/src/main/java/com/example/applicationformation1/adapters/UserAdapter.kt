package com.example.applicationformation1.adapters

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.applicationformation1.*
import com.example.applicationformation1.data.User

class UserAdapter(val activity: ListUsersFragment,
                  private val UserList: List<User>,
                  val callerActivity: MainActivity):
    RecyclerView.Adapter<UserAdapter.UserLine>(){

    inner class UserLine(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        //the card properties
        var nom: TextView
        var prenom: TextView
        var email: TextView
        var tel: TextView
        var photo: ImageView
        var callButton : Button
        var smsButton : Button

        init{
            nom = view.findViewById(R.id.userNom)
            prenom = view.findViewById(R.id.userPrenom)
            tel = view.findViewById(R.id.userPhone)
            email=view.findViewById(R.id.userEmail)
            photo=view.findViewById(R.id.listUserPhoto)
            callButton=view.findViewById(R.id.callUser)
            smsButton=view.findViewById(R.id.sendSmsUser)

            callButton?.setOnClickListener(this);
            smsButton?.setOnClickListener(this)
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id){

                //we click on checkBox we add the selected card to the list in order
                //to remove them from the list and the datababse
                callButton.id -> {

                    Log.e("Call button" , "Call user"+callButton.tag);
                    callPerson(callButton.tag.toString());

                }
                smsButton.id -> {

                    Log.e("Send SMS" , "Send SMS"+callButton.tag);
                    val sendSmsFragment = sendSmsFragment()
                    val args = Bundle()
                    //we save id of actual product in the tag of each card
                    args.putString("phone", v?.tag.toString())
                    sendSmsFragment.setArguments(args)
                    //launch the detail activity
                    callerActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.FragmentConnexionIns, sendSmsFragment, "send sms Fragment")
                        .commit()

                }
            }
        }


    }

    fun callPerson (phone:String){

        val permissions = arrayOf(
            "android.permission.CALL_PHONE"
        )
        //200 code demanding permission to use
        var requestCode = 200;
        requestPermissions(callerActivity, permissions, 200);

        val callIntent = Intent(Intent.ACTION_CALL);
        callIntent.data = Uri.parse("tel:"+phone)

        if(ActivityCompat.checkSelfPermission(callerActivity, Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED){
            return
        }
        startActivity(callerActivity, callIntent,null);

    }
    override fun onBindViewHolder(holder: UserLine, position: Int) {
        val user:User = UserList[position]
        var photoUrl="http://nticformation.com/"+user.photo
        Glide.with(activity).load(photoUrl).into(holder.photo)
        holder.nom.text="Nom :"+user.nom
        holder.prenom.text="Prenom :"+user.prenom
        holder.email.text="Email :"+user.email
        holder.tel.text="Tel :"+user.tel
        holder.callButton.setTag(user.tel);
        holder.smsButton.setTag(user.tel);

        //will used to delete or view the product
       // holder.mCardView.setTag(produit.id);
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserLine {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_user_row, parent, false)
        return UserLine(itemView)
    }
    //get the count of the produitList in the recylerView
    override fun getItemCount(): Int {
        return UserList.size
    }


}