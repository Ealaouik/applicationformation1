package com.example.applicationformation1


import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_send_sms.*
import kotlinx.android.synthetic.main.fragment_send_sms.view.*
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class sendSmsFragment : Fragment(), View.OnClickListener {

    var phone : String? = null;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var args: Bundle? = getArguments();

        var view =  inflater.inflate(R.layout.fragment_send_sms, container, false)

        phone = args?.getString("phone")
        Log.e("Phone", phone);
        view.envoyerSmsInscription.setOnClickListener(this);

        val permissions = arrayOf(
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.READ_PHONE_STATE",
            "android.permission.SYSTEM_ALERT_WINDOW",
            "android.permission.SEND_SMS"
        )
        //200 code demanding permission to use
        var requestCode = 200;
        requestPermissions(permissions, 200);
        view.envoyerSmsInscription.text = "Envoyer à "+phone


        return view
    }

    override fun onClick(v: View?) {
        if (v?.id == envoyerSmsInscription.id){

            sendSms()

        }
    }

    fun sendSms(){
        val tophone = phone;
        val smsMessage = messageSmsUser.text.toString()

        try{
            val smsManager: SmsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phone, null, smsMessage, null, null)
            Toast.makeText(activity,"SMS sent.", Toast.LENGTH_SHORT ).show()


        } catch(e:Exception){
            Toast.makeText(activity, "SMS failed.", Toast.LENGTH_SHORT).show();
        }

    }

}
