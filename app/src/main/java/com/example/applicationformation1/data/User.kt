package com.example.applicationformation1.data

data class User(var nom:String="",
                var prenom:String="",
                var email:String="",
                var tel:String="",
                var sexe:Char='M',
                var ville:String="",
                var photo:String="",
                var login:String="",
                var id:Int=0)