package com.example.applicationformation1.data

data class Produit(var libelle:String="",
                   var code:String="",
                   var description:String="",
                   var prix:Float=0f,
                   var prixVente:Float=0f,
                   var image:String="",
                   var marque:String="",
                   var  stock:Float=0f,
                   var listImage :String="",
                   var id:Int=0)
